const PgPool = require('pg').Pool;
const dbConfig = require('../config/database.js');

let pool;

async function initialize() {
  pool = new PgPool(dbConfig.hrPool);
}

async function close() {
  await pool.end();
}

function simpleExecute(statement, binds = []) {
  return new Promise(async (resolve, reject) => {
    try {
      const result = await pool.query(statement, binds);
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
}

module.exports.initialize = initialize;
module.exports.close = close;
module.exports.simpleExecute = simpleExecute;
