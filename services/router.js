const express = require('express');
const router = new express.Router();
const users = require('../controllers/users.js');
const transactions = require('../controllers/transactions.js');
const accounts = require('../controllers/accounts.js');
const statistics = require('../controllers/statistics.js');
const authMiddleware = require('../middlewares/authentication.js');

router.route('/users/reg').post(users.post);
router.route('/users/login').post(users.login);

router
  .use(authMiddleware.checkToken)
  .route('/transactions/categories')
  .get(transactions.getCategories);
router
  .use(authMiddleware.checkToken)
  .route('/transactions/:id?')
  .get(transactions.get)
  .post(transactions.post)
  .put(transactions.put)
  .delete(transactions.delete);

router
  .use(authMiddleware.checkToken)
  .route('/accounts/:id?')
  .get(accounts.get)
  .post(accounts.post)
  .put(accounts.put)
  .delete(accounts.delete);

router
  .use(authMiddleware.checkToken)
  .route('/statistics')
  .get(statistics.get);

module.exports = router;
