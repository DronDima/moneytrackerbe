const jwt = require('jsonwebtoken');
const config = require('../config/web-server.js');

function checkToken(req, res, next) {
  const token = req.headers.authorization;
  if (!token) {
    return res.status(401).send('Access denied. No token provided.');
  }
  try {
    const decoded = jwt.verify(token, config.myPrivateKey);
    req.user = decoded;
    next();
  } catch (err) {
    res.status(400).send('Invalid token.');
  }
}

module.exports.checkToken = checkToken;
