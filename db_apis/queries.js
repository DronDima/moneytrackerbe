const getAllTransactionsQuery = `SELECT transactions.id,
 category_name "category", accounts.name "account",
 expenses,
 income,
 transactions.currency,
 comment,
 date FROM transactions
  JOIN transaction_categories
  ON transactions.category_id = transaction_categories.id
  JOIN accounts
  ON transactions.account_id = accounts.id 
  WHERE transactions.user_id = $1::integer
  ORDER BY date DESC`;

const getTransactionQuery = `SELECT transactions.id,
 category_name "category", accounts.name "account",
 expenses,
 income,
 transactions.currency,
 comment,
 date FROM transactions
  JOIN transaction_categories
  ON transactions.category_id = transaction_categories.id
  JOIN accounts
  ON transactions.account_id = accounts.id
  WHERE transactions.id = $1::integer AND transactions.user_id = $2::integer`;

const createTransactionQuery = `INSERT INTO transactions (
 category_id,
 account_id,
 expenses,
 income,
 currency,
 comment,
 date,
 user_id)
 VALUES (
    (SELECT id FROM transaction_categories WHERE category_name = $1::varchar),
    (SELECT id FROM accounts WHERE name = $2::varchar),
    $3::numeric,
    $4::numeric,
    $5::varchar,
    $6::varchar,
    $7::timestamp,
    $8::integer)
 RETURNING id`;

const deleteTransactionQuery = `WITH deleted AS 
 (DELETE FROM transactions WHERE id = $1::integer AND user_id = $2::integer IS TRUE RETURNING *)
  SELECT count(*) FROM deleted`;

const updateTransactionQuery = `update transactions
  set category_id = (SELECT id FROM transaction_categories WHERE category_name = $1::varchar),
   account_id = (SELECT id FROM accounts WHERE name = $2::varchar),
   expenses = $3::numeric,
   income = $4::numeric,
   currency = $5::varchar,
   comment = $6::varchar,
   date = $7::timestamp
  WHERE user_id = $8::integer AND id = $9::integer`;

const getAllAccountsQuery = `SELECT accounts.id, name, accounts.currency, 
  COALESCE((SELECT SUM(income) FROM transactions WHERE account_id = accounts.id), 0) "income",
  COALESCE((SELECT SUM(expenses) FROM transactions WHERE account_id = accounts.id), 0) "expenses"
FROM accounts WHERE user_id = $1::integer`;

const getAccountQuery = `SELECT
  accounts.id,
  name,
  accounts.currency, 
  COALESCE((SELECT SUM(income) FROM transactions WHERE account_id = accounts.id), 0) "income",
  COALESCE((SELECT SUM(expenses) FROM transactions WHERE account_id = accounts.id), 0) "expenses"
FROM accounts
WHERE accounts.id = $1::integer AND user_id = $2::integer`;

const createAccountQuery = `INSERT INTO accounts (
  name,
  currency,
  user_id)
  VALUES (
     $1::varchar,
     $2::varchar,
     $3::integer)
  RETURNING id`;

const updateAccountQuery = `update accounts
  set name = $1::varchar,
  currency = $2::varchar
WHERE user_id = $3::integer AND id = $4::integer`;

const deleteAccountQuery = `WITH deleted AS 
 (DELETE FROM accounts WHERE id = $1::integer AND user_id = $2::integer IS TRUE RETURNING *)
  SELECT count(*) FROM deleted`;

const getStatisticsQuery = `SELECT 
  (SELECT category_name FROM transaction_categories WHERE id = category_id),
  SUM(expenses) "expenses"
FROM transactions
  JOIN transaction_categories ON category_id = transaction_categories.id
  WHERE expenses <> 0 AND transactions.user_id = $1::integer
  GROUP BY category_id`;

const createUserQuery = `INSERT INTO users (
    login,
    password)
    VALUES (
       $1::varchar,
       $2::varchar)
    RETURNING id`;

const getAllCategoriesQuery = `SELECT category_name FROM transaction_categories`;

module.exports.getAllTransactionsQuery = getAllTransactionsQuery;
module.exports.getTransactionQuery = getTransactionQuery;
module.exports.createTransactionQuery = createTransactionQuery;
module.exports.deleteTransactionQuery = deleteTransactionQuery;
module.exports.updateTransactionQuery = updateTransactionQuery;

module.exports.getAllAccountsQuery = getAllAccountsQuery;
module.exports.getAccountQuery = getAccountQuery;
module.exports.createAccountQuery = createAccountQuery;
module.exports.updateAccountQuery = updateAccountQuery;
module.exports.deleteAccountQuery = deleteAccountQuery;

module.exports.getStatisticsQuery = getStatisticsQuery;

module.exports.createUserQuery = createUserQuery;

module.exports.getAllCategoriesQuery = getAllCategoriesQuery;
