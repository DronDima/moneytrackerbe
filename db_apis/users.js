const database = require('../services/database.js');
const queries = require('./queries.js');

const baseQuery = `select id,
    login
  from users`;

async function findById(context) {
  let query = baseQuery;
  const binds = [];

  if (context.id) {
    binds.push(context.id);

    query += `\nwhere id = $1::integer`;
  }

  const result = await database.simpleExecute(query, binds);

  return result.rows;
}

async function findByLogin(context) {
  let query = baseQuery;
  const binds = [];
  binds.push(context.login);
  query = `SELECT id, login, password
    FROM users
    WHERE login = $1::varchar`;

  const result = await database.simpleExecute(query, binds);
  return result.rows;
}

async function createUser(user) {
  const binds = Object.values(user);

  const result = await database.simpleExecute(queries.createUserQuery, binds);

  delete user.password;
  user.id = result.rows[0].id;

  return user;
}

module.exports.findById = findById;
module.exports.findByLogin = findByLogin;
module.exports.createUser = createUser;
