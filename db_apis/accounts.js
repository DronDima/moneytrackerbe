const database = require('../services/database.js');
const queries = require('./queries.js');
const pg = require('pg');

async function getAccounts(context) {
  binds = [];
  let query = context.id
    ? queries.getAccountQuery
    : queries.getAllAccountsQuery;
  if (context.id) {
    binds.push(context.id);
  }
  binds.push(context.userId);

  const result = await database.simpleExecute(query, binds);

  return result.rows;
}

async function createAccount(account) {
  const binds = Object.values(account);
  const result = await database.simpleExecute(
    queries.createAccountQuery,
    binds,
  );

  account.id = result.rows[0].id;

  return account;
}

async function updateAccount(account) {
  binds = Object.values(account);
  const result = await database.simpleExecute(
    queries.updateAccountQuery,
    binds,
  );
  if (result.rowCount === 1) {
    return account;
  } else {
    return null;
  }
}

async function deleteAccount(context) {
  const binds = Object.values(context);
  const result = await database.simpleExecute(
    queries.deleteAccountQuery,
    binds,
  );
  return result.rows[0].count == 1;
}

module.exports.getAccounts = getAccounts;
module.exports.createAccount = createAccount;
module.exports.updateAccount = updateAccount;
module.exports.deleteAccount = deleteAccount;
