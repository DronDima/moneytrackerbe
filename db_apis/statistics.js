const database = require('../services/database.js');
const queries = require('./queries.js');

async function getStatistics(context) {
  const binds = Object.values(context);
  const result = await database.simpleExecute(
    queries.getStatisticsQuery,
    binds,
  );

  return result.rows;
}

module.exports.getStatistics = getStatistics;
