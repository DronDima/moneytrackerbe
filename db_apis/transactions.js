const database = require('../services/database.js');
const queries = require('./queries.js');
const pg = require('pg');

async function getTransactions(context) {
  binds = [];
  let query = context.id
    ? queries.getTransactionQuery
    : queries.getAllTransactionsQuery;
  if (context.id) {
    binds.push(context.id);
  }
  binds.push(context.userId);

  const result = await database.simpleExecute(query, binds);

  return result.rows;
}

async function createTransaction(transaction) {
  const binds = Object.values(transaction);

  const result = await database.simpleExecute(
    queries.createTransactionQuery,
    binds,
  );

  transaction.id = result.rows[0].id;

  return transaction;
}

async function deleteTransaction(context) {
  const binds = Object.values(context);
  const result = await database.simpleExecute(
    queries.deleteTransactionQuery,
    binds,
  );
  return result.rows[0].count == 1;
}

async function updateTransaction(transaction) {
  binds = Object.values(transaction);
  const result = await database.simpleExecute(
    queries.updateTransactionQuery,
    binds,
  );
  if (result.rowCount === 1) {
    return transaction;
  } else {
    return null;
  }
}

async function getCategories() {
  binds = [];
  let query = queries.getAllCategoriesQuery;

  const result = await database.simpleExecute(query, binds);

  return result.rows;
}

module.exports.getTransactions = getTransactions;
module.exports.createTransaction = createTransaction;
module.exports.deleteTransaction = deleteTransaction;
module.exports.updateTransaction = updateTransaction;
module.exports.getCategories = getCategories;
