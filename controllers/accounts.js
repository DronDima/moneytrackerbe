const accountsAPI = require('../db_apis/accounts.js');

async function get(req, res, next) {
  try {
    const context = {};
    context.id = parseInt(req.params.id, 10);
    context.userId = parseInt(req.user.id, 10);

    const rows = await accountsAPI.getAccounts(context);
    if (req.params.id) {
      if (rows.length === 1) {
        res.status(200).json(rows[0]);
      } else {
        res
          .status(404)
          .send(
            'Transaction not found or user dont have access for this account.',
          );
      }
    } else {
      res.status(200).json(rows);
    }
  } catch (err) {
    next(err);
  }
}

function getAccountInfoFromReq(req) {
  const accountInfo = {
    name: req.body.name,
    currency: req.body.currency,
    userId: req.user.id,
  };
  if (req.body.id) {
    accountInfo.id = req.body.id;
  }
  return accountInfo;
}

async function post(req, res, next) {
  try {
    let accountInfo = getAccountInfoFromReq(req);

    account = await accountsAPI.createAccount(accountInfo);

    res.status(201).json(account);
  } catch (err) {
    next(err);
  }
}

async function put(req, res, next) {
  try {
    let accountInfo = getAccountInfoFromReq(req);

    account = await accountsAPI.updateAccount(accountInfo);

    if (account !== null) {
      res.status(200).json(account);
    } else {
      res
        .status(404)
        .send(
          'Transaction not found or user dont have access for this account.',
        );
    }
  } catch (err) {
    next(err);
  }
}

async function del(req, res, next) {
  try {
    const context = {};
    context.id = parseInt(req.params.id, 10);
    context.userId = parseInt(req.user.id, 10);

    const success = await accountsAPI.deleteAccount(context);

    if (success) {
      res.status(204).end();
    } else {
      res
        .status(404)
        .send(
          'Transaction not found or user dont have access for this account.',
        );
    }
  } catch (err) {
    next(err);
  }
}

module.exports.get = get;
module.exports.post = post;
module.exports.put = put;
module.exports.delete = del;
