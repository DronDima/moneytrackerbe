const transactionsAPI = require('../db_apis/transactions.js');

function transformDBOutput(rows) {
  result = [];
  rows.forEach(el => {
    const index = result.findIndex(resEl => {
      if (
        el.date.getDate() === resEl.date.getDate() &&
        el.date.getMonth() === resEl.date.getMonth() &&
        el.date.getFullYear() === resEl.date.getFullYear()
      ) {
        return true;
      }
    });
    if (index === -1) {
      result.push({
        data: [el],
        date: el.date,
      });
    } else {
      result[index].data.push(el);
    }
  });
  return result;
}

async function get(req, res, next) {
  try {
    const context = {};
    context.id = parseInt(req.params.id, 10);
    context.userId = parseInt(req.user.id, 10);

    const rows = await transactionsAPI.getTransactions(context);
    if (req.params.id) {
      if (rows.length === 1) {
        res.status(200).json(rows[0]);
      } else {
        res
          .status(404)
          .send(
            'Transaction not found or user dont have access for this transaction.',
          );
      }
    } else {
      res.status(200).json(transformDBOutput(rows));
    }
  } catch (err) {
    next(err);
  }
}

function getTransactionInfoFromReq(req) {
  const transactionInfo = {
    category: req.body.category,
    account: req.body.account,
    expenses: req.body.expenses,
    income: req.body.income,
    currency: req.body.currency,
    comment: req.body.comment,
    date: req.body.date,
    userId: req.user.id,
  };
  if (req.body.id) {
    transactionInfo.id = req.body.id;
  }
  return transactionInfo;
}

async function post(req, res, next) {
  try {
    let transactionInfo = getTransactionInfoFromReq(req);

    transaction = await transactionsAPI.createTransaction(transactionInfo);

    res.status(201).json(transaction);
  } catch (err) {
    next(err);
  }
}

async function del(req, res, next) {
  try {
    const context = {};
    context.id = parseInt(req.params.id, 10);
    context.userId = parseInt(req.user.id, 10);

    const success = await transactionsAPI.deleteTransaction(context);

    if (success) {
      res.status(204).end();
    } else {
      res
        .status(404)
        .send(
          'Transaction not found or user dont have access for this transaction.',
        );
    }
  } catch (err) {
    next(err);
  }
}

async function put(req, res, next) {
  try {
    let transactionInfo = getTransactionInfoFromReq(req);

    transaction = await transactionsAPI.updateTransaction(transactionInfo);

    if (transaction !== null) {
      res.status(200).json(transaction);
    } else {
      res
        .status(404)
        .send(
          'Transaction not found or user dont have access for this transaction.',
        );
    }
  } catch (err) {
    next(err);
  }
}

async function getCategories(req, res, next) {
  try {
    const context = {};

    const rows = await transactionsAPI.getCategories();
    res.status(200).json(rows.map(el => el.category_name));
  } catch (err) {
    next(err);
  }
}

module.exports.get = get;
module.exports.post = post;
module.exports.delete = del;
module.exports.put = put;
module.exports.getCategories = getCategories;
