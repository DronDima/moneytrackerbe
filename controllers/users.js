const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const usersAPI = require('../db_apis/users.js');
const config = require('../config/web-server.js');

async function get(req, res, next) {
  try {
    const context = {};

    context.id = parseInt(req.params.id, 10);

    const rows = await usersAPI.findById(context);

    if (req.params.id) {
      if (rows.length === 1) {
        res.status(200).json(rows[0]);
      } else {
        res.status(404).end();
      }
    } else {
      res.status(200).json(rows);
    }
  } catch (err) {
    next(err);
  }
}

function generateAuthToken(user) {
  const token = jwt.sign({ id: user.id }, config.myPrivateKey);
  return token;
}

async function post(req, res, next) {
  try {
    let userInfo = {
      login: req.body.login,
      password: req.body.password,
    };

    const rows = await usersAPI.findByLogin(userInfo);

    if (rows.length === 1) {
      return res.status(400).send('User already registered.');
    }

    userInfo.password = await bcrypt.hash(userInfo.password, 10);
    user = await usersAPI.createUser(userInfo);
    const token = generateAuthToken(user);
    res
      .status(201)
      .header('authorization', token)
      .json(user);
  } catch (err) {
    next(err);
  }
}

async function login(req, res, next) {
  try {
    let userInfo = {
      login: req.body.login,
      password: req.body.password,
    };

    const rows = await usersAPI.findByLogin(userInfo);
    userInfo.id = rows[0].id;

    if (rows.length === 1) {
      const match = await bcrypt.compare(userInfo.password, rows[0].password);
      if (match) {
        const token = generateAuthToken(userInfo);
        delete userInfo.password;
        return res
          .status(200)
          .header('authorization', token)
          .json(userInfo);
      }
    }
    return res.status(401).send('Bad credentials.');
  } catch (err) {
    next(err);
  }
}

module.exports.get = get;
module.exports.post = post;
module.exports.login = login;
