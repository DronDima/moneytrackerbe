const statisticsAPI = require('../db_apis/statistics.js');

async function get(req, res, next) {
  try {
    const context = {};
    context.userId = parseInt(req.user.id, 10);

    const rows = await statisticsAPI.getStatistics(context);

    res.status(200).json(rows);
  } catch (err) {
    next(err);
  }
}

module.exports.get = get;
